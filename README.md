# Siculix Docker
Siculix container docker

```
xhost +"local:docker@"
docker run --rm -v /tmp/.X11-unix:/tmp/.X11-unix -r DISPLAY=${DISPLAY} registry.gitlab.com/nicolalandro/siculix-docker/siculix:2.0.5
```

## Run dev
```
xhost +"local:docker@"
docker-compose up
```