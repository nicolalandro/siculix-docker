FROM gradle:jdk17

# GET SICULIX jar
RUN apt-get update && apt-get install -y wget
WORKDIR /siculix
RUN wget https://launchpad.net/sikuli/sikulix/2.0.5/+download/sikulixide-2.0.5-lux.jar
RUN apt-get update && apt-get install -y libxext-dev libfontconfig1 libxrender1 libxtst6 libxi6 libgconf-2-4

CMD java -jar /siculix/sikulixide-2.0.5-lux.jar
